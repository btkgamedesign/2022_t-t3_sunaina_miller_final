using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] public float maxHealth; //gets accessed by player manager script
    [SerializeField] public float currentHealth; //gets accessed by player manager script
    [SerializeField] protected ImpactEffect impactEffect;

    public void Start()
    {
        currentHealth = maxHealth;
        //Debug.Log($"Setting current health ({currentHealth}) to maximum health {maxHealth}.");
    }

    public void Death()
    {
        currentHealth = 0f;
        //Debug.Log($"Dying because health is at {currentHealth}");
        impactEffect.ImpactMatChange();
        Destroy(gameObject);
    }
}
