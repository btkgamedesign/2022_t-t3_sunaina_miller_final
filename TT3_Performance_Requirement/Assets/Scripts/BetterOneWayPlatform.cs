using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterOneWayPlatform : MonoBehaviour
{
    PlatformEffector2D effector;
    float waitTime; //waiting short amount of time; player has to hold S for certain amount of time to be able to go down from one-way platform

    void Start()
    {
        effector = GetComponent<PlatformEffector2D>(); //getting platform effector component from this game object    
    }

    void Update()
    {
        //if S has been released, reset wait time
        if(Input.GetKeyUp(KeyCode.S))
        {
            waitTime = 0.1f;
        }

        if(Input.GetKey(KeyCode.S))
        {
            //if S has been pressed for certain amount of time, then let the player jump down from one-way platform
            if(waitTime <= 0)
            {
                effector.rotationalOffset = 180f;
                waitTime = 0.1f;
            }
            else
            {
                waitTime -= Time.deltaTime; //decrease wait time
            }
        }

        if(Input.GetKey(KeyCode.Space))
        {
            effector.rotationalOffset = 0f;
        }
    }
}
