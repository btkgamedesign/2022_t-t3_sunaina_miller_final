using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactEffect : MonoBehaviour
{
    [SerializeField] public Material matWhite;
    [SerializeField] public Material matRed;
    public Material matDefault;
    [SerializeField] public SpriteRenderer spriteRenderer;

    void Start()
    {
        matWhite = Resources.Load("WhiteMat", typeof(Material)) as Material;
        matDefault = spriteRenderer.material;
    }

    public IEnumerator ImpactMatChange()
    {
        //anim.Play("EnemyDeath");
        spriteRenderer.material = matWhite;
        
        Debug.Log("Waiting to change to red.");
        yield return new WaitForSeconds(0.3f);

        spriteRenderer.material = matRed;
        Debug.Log("Waiting to change to destroy enemy.");
        yield return new WaitForSeconds(0.1f);

        spriteRenderer.material = matDefault;
        //Destroy(gameObject);
    }
}
