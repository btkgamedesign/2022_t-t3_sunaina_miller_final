using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHeartSystem : MonoBehaviour
{
    [SerializeField] GameObject heartPrefab; //regular heart
    [SerializeField] GameObject goldenHeartPrefab; //extra heart
    public PlayerManager playerManager;
    bool goldenHeartSpawned = false;

    public void DrawHeart (float hearts, float maxHearts) //
    {
        foreach (Transform child in transform) //looping through children of heart container (so the hearts) & destroying them (otherwise more hearts will be added)
        {
            Destroy(child.gameObject);
        }

        for (int i = 1; i < maxHearts; i++) //keep looping as long as the max hearts hasn't been reached
        {
            Debug.Log("i = " + i);
            Debug.Log("maxHearts = " + maxHearts);

            //spawning regular hearts
            if (i <= hearts)
            {
                GameObject heart = Instantiate(heartPrefab, transform.position, Quaternion.identity); //instatiate heart prefab & calling it heart
                heart.transform.SetParent(transform, false); //making the new heart a child of the heart container
            }
        }

        //spawning golden hearts
        if (playerManager.hasPaperCutUpgrade || playerManager.hasPaperWeightUpgrade)//if upgrade was collected, do this
        {
            GameObject goldenHeart = Instantiate(goldenHeartPrefab, transform.position, Quaternion.identity); //instatiate heart prefab & calling it heart
            goldenHeart.transform.SetParent(transform, false); //making the new heart a child of the heart container
            goldenHeartSpawned = true;
        }
    }

    /*
    public void DrawGoldenHeart(float hearts, float maxHearts) //
    {
        foreach (Transform child in transform) //looping through children of heart container (so the hearts) & destroying them (otherwise more hearts will be added)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < maxHearts; i++) //keep looping as long as the max hearts hasn't been reached
        {
            if (i + 1 <= hearts)
            {
                GameObject goldenHeart = Instantiate(goldenHeartPrefab, transform.position, Quaternion.identity); //instatiate heart prefab & calling it heart
                goldenHeart.transform.parent = transform; //making the new heart a child of the heart container
            }
        }
    }*/
}
