using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    [SerializeField] float bulletSpeed = 15f;
    [SerializeField] float bulletDamage = 0.5f;
    [SerializeField] Rigidbody2D rb;

    void FixedUpdate()
    {
        rb.velocity = transform.right * bulletSpeed; //constantly move to the right
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject); //destroy self if you come in contact with anything
    }
}
