using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookScoreItem : MonoBehaviour
{
    [SerializeField] int addToScore; //how much the item should add to score

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Score.scoreValue += addToScore;
            Debug.Log("Touched by player. Destroying self");
            Destroy(gameObject);
        }
    }
}
