using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUpgradePaperWeight : PlayerMovement
{
    new void Start()
    {
        originalMovementSpeed = movementSpeed; //assigning original movement speed the movement speed
        initialGravityScale = rb.gravityScale; //setting beginning gravity scale to rigidbody's gravity scale.
        transform.localScale = new Vector3(2f, 2f, 2f);
    }
}
