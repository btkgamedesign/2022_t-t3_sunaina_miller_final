using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUpgradePaperCut : PlayerMovement
{
    [SerializeField] float fireRate = 0.2f;
    [SerializeField] Transform firingPoint;
    public GameObject bulletPrefab;
    float timeUntilFire; //time between when you can shoot again

    new void Start()
    {
        originalMovementSpeed = movementSpeed; //saving my original movement speed
        initialGravityScale = rb.gravityScale;
        transform.localScale = new Vector3(2f, 2f, 2f);
    }

    public override void Update()
    {
        base.Update();

        #region Shooting
        /*mousePosition = mainCam.ScreenToWorldPoint(Input.mousePosition); //convert mouse position into actual coordinates
        Vector2 aimDirection = mousePosition - rb.position;*/

        if (Input.GetMouseButtonDown(0) && timeUntilFire < Time.time)
        {
            Shoot();
            timeUntilFire = Time.time + fireRate;
        }
        #endregion
    }

    void Shoot()
    {
        float angle = isFacingRight ? 0f : 180f; //if facing right then shoot from angle of 0, if not shoot from the other side
        Instantiate(bulletPrefab, firingPoint.position, Quaternion.Euler(new Vector3(0f, 0f, angle))); //instantiating bullet
        Debug.Log($"Shooting bullet.");
    }
}
