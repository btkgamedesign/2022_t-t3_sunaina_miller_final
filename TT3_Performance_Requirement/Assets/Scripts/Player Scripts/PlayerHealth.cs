using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health
{
    [SerializeField] UIHeartSystem heartSystem;
    [SerializeField] PlayerManager playerManager;
    [SerializeField] GameObject respawnPoint;

    new void Start()
    {
        currentHealth = 3;
        heartSystem.DrawHeart(currentHealth, maxHealth);
        transform.localScale = new Vector3(1f, 1f, 1f);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            TakeDamage();
            Debug.Log("Taking damage from Player Health script.");
        }
    }

    public void TakeDamage()
    {
        if (currentHealth > 0) //if current health is above 0, take damage
        {
            impactEffect.ImpactMatChange();
            currentHealth--;
            Debug.Log($"Taking damage. Current Health at {currentHealth}.");
            playerManager.Start(); //disabling any upgrades

            if (currentHealth <= 0)
            {
                Death();
            }
        }
        else //if current health is anything but above 0 (so 0 or -1) then die
        {
            Death();
        }

        heartSystem.DrawHeart(currentHealth, maxHealth);
    }

    public void ExtraLife()
    {
        if (currentHealth < maxHealth)
        {
            currentHealth++;
            heartSystem.DrawHeart(currentHealth, maxHealth);
        }
    }

    new void Death()
    {
        transform.position = respawnPoint.transform.position;
        Start();
    }
}
