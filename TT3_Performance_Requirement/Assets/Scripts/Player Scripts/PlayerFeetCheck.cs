using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFeetCheck : MonoBehaviour
{
    [SerializeField] Rigidbody2D footRb;
    public PlayerManager playerManager;

    void OnTriggerEnter2D(Collider2D collision)
    {   
        //if player feet collide with ink enemy head, bounce & kill enemy
        if (collision.tag == "InkEnemyHead")
        {
            Debug.Log($"{gameObject} collided with {collision}.");
            EnemyHealth death = collision.transform.parent.gameObject.GetComponent<EnemyHealth>(); //accessing enemy health from parent
            death.Death();
            Debug.Log($"Death");
            //giving a bounce
            footRb.velocity = new Vector2(footRb.velocity.x, 0f);
            footRb.AddForce(Vector2.up * 300f);
        }

        if (collision.tag == "ScissorsEnemyHead")
        {
            //damage gets taken in another script
            //giving a bounce
            footRb.velocity = new Vector2(footRb.velocity.x, 0f);
            footRb.AddForce(Vector2.up * 300f);

            if (playerManager.hasPaperCutUpgrade)
            {
                EnemyHealth death = collision.transform.parent.gameObject.GetComponent<EnemyHealth>(); //accessing enemy health from parent
                death.Death();
                Debug.Log($"Death");
            }
        }

        /*if (collision.tag == "ScissorsEnemy Head" && Upgrade B (Paper Weights enabled)
        {

        }*/
    }
}
