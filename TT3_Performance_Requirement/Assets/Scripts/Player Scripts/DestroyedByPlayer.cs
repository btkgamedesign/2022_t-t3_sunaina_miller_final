using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyedByPlayer : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("Touched by player. Destroying self");
            Destroy(gameObject);
        }
    }
}