using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    //collision detection
    //upgrade manager

    //both bools get accessed by UI Heart System script
    [HideInInspector] public PlayerUpgradePaperCut playerUpgradePaperCut;
    [SerializeField] public bool hasPaperCutUpgrade = false;

    [HideInInspector] public PlayerUpgradePaperWeight playerUpgradePaperWeight;
    [SerializeField] public bool hasPaperWeightUpgrade = false;

    [HideInInspector] public PlayerHealth playerHealth;
    [HideInInspector] public PlayerMovement playerMovement;

    [SerializeField] Texture2D defaultCursor;
    [SerializeField] Texture2D targetCursor;

    bool isPaused;

    public void Start() //gets accessed in Player Health script
    {
        //switching off all upgrades, setting everything to default settings
        hasPaperCutUpgrade = false;
        hasPaperWeightUpgrade = false;
        playerUpgradePaperCut.enabled = false;
        playerUpgradePaperWeight.enabled = false;
        Debug.Log($"Disabling any upgrades.");

        transform.localScale = new Vector3(1f, 1f, 1f);
        playerMovement.enabled = true;
        Score.scoreValue = 0;
        DefaultCursorSettingsOff();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "PaperCut")
        {
            Debug.Log("Received Paper Cut upgrade");
            PaperCutUpgradeCollected();
        }

        if (collision.gameObject.tag == "PaperWeight")
        {
            Debug.Log("Received Paper Weight upgrade");
            PaperWeightUpgradeCollected();
        }

        /*if (collision.gameObject.tag == "PaperWeight")
        {
            Debug.Log("Received Paper Weight upgrade");
        }*/
    }

    void PaperCutUpgradeCollected()
    {
        hasPaperCutUpgrade = true; //player has paper cut upgrade
        playerUpgradePaperCut.enabled = true; //enabling my paper cut script

        hasPaperWeightUpgrade = false; //cancels out other upgrade
        playerUpgradePaperWeight.enabled = false; //disabling other upgrade script

        playerHealth.ExtraLife(); //giving player extra life
        Debug.Log($"{gameObject} has {playerHealth.currentHealth} health.");

        playerMovement.enabled = false;
        DefaultCursorSettingsOff();
    }

    void PaperWeightUpgradeCollected()
    {
        hasPaperWeightUpgrade = true; //player has paper cut upgrade
        playerUpgradePaperWeight.enabled = true; //enabling my paper cut script

        hasPaperCutUpgrade = false; //cancels out other upgrade
        playerUpgradePaperCut.enabled = false; //disabling other upgrade script

        playerHealth.ExtraLife(); //giving player extra life
        Debug.Log($"{gameObject} has {playerHealth.currentHealth} health.");

        playerMovement.enabled = false;

        DefaultCursorSettingsOff();
    }

    public void DefaultCursorSettingsOff() //gets accessed by PauseMenu script
    {
        Cursor.visible = false; //hiding cursor
        Cursor.lockState = CursorLockMode.Locked; //confining cursor to centre
        Cursor.SetCursor(defaultCursor, new Vector2(0, 0), CursorMode.Auto); //setting cursor image to defaul one
    }

    public void DefaultCursorSettingOn() //gets accessed by PauseMenu script
    {
        Cursor.visible = true; //making cursor visible
        Cursor.lockState = CursorLockMode.Confined; //confining cursor to game window
        Cursor.SetCursor(defaultCursor, new Vector2(0, 0), CursorMode.Auto); //setting cursor image to defaul one
    }

    public void TargetCrosshairOn()
    {
        Cursor.visible = true; //making cursor visible
        Cursor.lockState = CursorLockMode.Confined; //confining cursor to game window
        Cursor.SetCursor(targetCursor, new Vector2(0, 0), CursorMode.Auto); //change cursor to target crosshair
    }
}
