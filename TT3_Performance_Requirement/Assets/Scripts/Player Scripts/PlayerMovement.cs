using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    #region Movement
    public float movementSpeed; //player speed
    public Rigidbody2D rb; //will help add velocity & forces to player
    protected float movementOnX; //movement on x axis
    public bool isFacingRight;
    bool isRunning;

    protected float originalMovementSpeed;
    #endregion

    #region Gliding
    [SerializeField] protected float glidingSpeed;
    protected float initialGravityScale; //gravity at beginning
    public LayerMask groundLayers;
    #endregion

    #region Jump
    public float jumpForce = 20f;
    public Transform p_feet;
    #endregion

    [SerializeField] GameObject respawnPoint; //object locations is respawn point

    public void Start() //gets accessed by PlayerManager script, so needs to be public
    {
        originalMovementSpeed = movementSpeed; //assigning original movement speed the movement speed
        initialGravityScale = rb.gravityScale; //setting beginning gravity scale to rigidbody's gravity scale.
        //transform.position = respawnPoint.transform.position;
        Debug.Log("Sarting");
        Cursor.visible = false; //hiding cursor
        Cursor.lockState = CursorLockMode.Locked; //confining cursor to centre
        Debug.Log($"Current movement speed {movementSpeed}, original speed {originalMovementSpeed}");
    }

    public virtual void Update()
    {
        movementOnX = Input.GetAxisRaw("Horizontal"); //getting the axis, with no smooth filtering; "horizontal" accesses X axis

        if (Input.GetButtonDown("Jump") && isGrounded()) //once grounded AND button pressed, then you can jump
            Jump();

        if(Input.GetKey(KeyCode.LeftShift))
        {
            movementSpeed = originalMovementSpeed + 5;
            Debug.Log($"Current movement speed {movementSpeed}");
        }
        
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            movementSpeed = originalMovementSpeed; //setting movement speed back to its original speed
            Debug.Log($"Current movement speed {movementSpeed}");
        }
        

        #region Gliding
        var glidingInput = Input.GetButton("Jump"); //bool gliding is true when jump button (space) held down

        if (glidingInput && rb.velocity.y <= 0) //if gliding button pressed & if vertical velocity less then 0, chara is falling
        {
            rb.gravityScale = 0; //gravity scale must be set 0 for proper gliding behaviour
            rb.velocity = new Vector2(rb.velocity.x, -glidingSpeed);
        }
        else
        {
            rb.gravityScale = initialGravityScale;
        }
        #endregion
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(movementOnX * movementSpeed, rb.velocity.y); //horizontal movement of player
        #region Facing Left or Right
        if (movementOnX < 0 && isFacingRight)
        {
            FlipSprite();
        }
        else if (movementOnX > 0 && !isFacingRight)
        {
            FlipSprite();
        }
        #endregion
    }

    protected void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        //jump animation & sound
    }

    public bool isGrounded()
    {
        Collider2D groundCheck = Physics2D.OverlapCircle(p_feet.position, 0.5f, groundLayers); //creating a circle where player feet are to detect ground collision

        if(groundCheck != null)
        {
            return true;
        }
        return false;
    }

    protected void FlipSprite()
    {
        isFacingRight = !isFacingRight; //flipping sprite
        transform.Rotate(0f, 180f, 0f);
    }

    void OnTriggerEnter2D(Collider2D other) //entering death box makes player go to respawn point
    {
        if (other.tag == "Death")
        {
            Debug.Log("Entered death box.");
            transform.position = respawnPoint.transform.position;
        }
    }
}
