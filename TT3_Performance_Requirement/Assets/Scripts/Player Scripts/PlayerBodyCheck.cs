using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBodyCheck : MonoBehaviour
{
    [SerializeField] Rigidbody2D footRb;
    [SerializeField] PlayerMovement playerMovement;

    void OnTriggerEnter2D(Collider2D collision)
    {
        bool isFacingRight = playerMovement.isFacingRight;
        //if player collides with Ink enemy's body or Scissors enemy, then damage is taken
        if (collision.tag == "InkEnemy" || collision.tag == "ScissorsEnemy" || collision.tag == "ScissorsEnemyHead")
        {
            Debug.Log($"{gameObject} came in contact with {collision}.");
            PlayerHealth damage = transform.parent.gameObject.GetComponent<PlayerHealth>(); //accessing enemy health from parent
            damage.TakeDamage();

            
            //footRb.AddForce(Vector2.up * 300f);

            //want to add force to bounce player away from enemy when they touch them, bc player can walk into enemy, take damage, jump, and kill enemy

            /*footRb.velocity = new Vector2(footRb.velocity.x, 0f);
            footRb.AddForce(-Vector2.right * -300f);*/

        }
    }
}
