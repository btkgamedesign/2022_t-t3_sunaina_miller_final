using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;
    public bool isPaused;

    [SerializeField] PlayerManager playerManager;
    [SerializeField] Texture2D defaultCursor;
    [SerializeField] Texture2D targetCursor;

    void Start()
    {
        Resume();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused) //if game is already paused, resume game
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    //pause game by making menu appear & stopping time in game
    public void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
        playerManager.DefaultCursorSettingOn();
    }

    //resume game by hiding pause menu & setting time back to normal
    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;

        if (playerManager.hasPaperCutUpgrade) //if paper cut upgrade is active, show the cursor
        {
            playerManager.TargetCrosshairOn();
        }
        else
        {
            playerManager.DefaultCursorSettingsOff();
        }
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu"); //load scene by the name MainMenu
    }
}
