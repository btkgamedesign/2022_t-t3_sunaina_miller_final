using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextSceneLoader : MainMenu
{
    public GameObject creditsMenu;
    
    void OnTriggerEnter2D(Collider2D collision)
    {
        //if we're in Level03, load credits
        Scene scene = SceneManager.GetActiveScene();
        if (collision.tag == "Player" && scene.name == "Level03")
        {
            creditsMenu.SetActive(true);
            Invoke("LoadMainMenu", 10);
        }
        else if (collision.tag == "Player")
        {
            Play();
            Debug.Log("Loading next scene");
        }
    }

    void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        creditsMenu.SetActive(false);
    }
}
