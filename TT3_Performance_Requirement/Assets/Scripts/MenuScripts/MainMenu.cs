using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    void Start()
    {
        Cursor.visible = true; //making cursor visible
        Cursor.lockState = CursorLockMode.Confined; //confining cursor to game window
    }

    public void Play()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); //takes current scene and goes for the scene after it in the build settins
    }

    public void Quit()
    {
        Application.Quit();
    }
}
