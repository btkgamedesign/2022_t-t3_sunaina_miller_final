using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBehaviour : MonoBehaviour
{
    public Camera cam;
    public Transform subject; //player
    Vector2 startPosition;
    float startZ;

    Vector2 travel => (Vector2)cam.transform.position - startPosition; //distance cam has moved since our original position
    //Vector2 parallaxFactor;

    float distanceFromSubject => transform.position.z - subject.position.z; //distance to player

    //is the distance from subject greater than 0, if so look at far clipping plane, if not look at near clipping plane.
    float clippingPlane => (cam.transform.position.z + (distanceFromSubject > 0 ? cam.farClipPlane : cam.nearClipPlane));
    float parallaxFactor => Mathf.Abs(distanceFromSubject) / clippingPlane; //absolute value

    public void Start()
    {
        startPosition = transform.position; //because it is Vector2, only stores x & y
        startZ = transform.position.z; //so we have to store z separately
    }

    public void Update()
    {
        Vector2 newPos = startPosition + travel * parallaxFactor;
        transform.position = new Vector3(newPos.x, newPos.y, startZ);
    }
}
