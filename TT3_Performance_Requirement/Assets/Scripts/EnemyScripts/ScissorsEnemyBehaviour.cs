using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScissorsEnemyBehaviour : MonoBehaviour
{
    //Time between patrols
    float waitTime;
    public float startWaitTime;

    public float walkingSpeed;
    [SerializeField] Transform[] patrolPoints; //points from which the enemy can move from
    int randomPatrolPoint;

    void Start()
    {
        randomPatrolPoint = Random.Range(0, patrolPoints.Length); //choose one of the patrol points
    }

    /*void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, patrolPoints[randomPatrolPoint].position, walkingSpeed * Time.deltaTime); //make enemy go to random patrol point

        if (Vector2.Distance(transform.position, patrolPoints[randomPatrolPoint].position) < 0.2f) //check distance enemy has to its patrol point, then move it to another position
        {
            if (waitTime <= 0) //if wait time is over
            {
                randomPatrolPoint = Random.Range(0, patrolPoints.Length); //choose one of the patrol points
                waitTime = startWaitTime;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }

        }
    }*/

    /*
    void FixedUpdate()
    {
        rb.velocity = new Vector2(movementOnX * movementSpeed, rb.velocity.y); //horizontal movement of player
        #region Facing Left or Right
        if (movementOnX < 0 && isFacingRight)
        {
            FlipSprite();
        }
        else if (movementOnX > 0 && !isFacingRight)
        {
            FlipSprite();
        }
        #endregion
    }

    void FlipSprite()
    {
        isFacingRight = !isFacingRight; //flipping sprite
        transform.Rotate(0f, 180f, 0f);
    }*/
}
