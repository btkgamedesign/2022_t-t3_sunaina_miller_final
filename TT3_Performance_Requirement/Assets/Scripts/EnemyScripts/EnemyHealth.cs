using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : Health
{
    public BoxCollider2D playerBodyCollider;
    public BoxCollider2D playerFeetCollider;
    public BoxCollider2D enemyHeadCollider;
    public BoxCollider2D enemyBodyCollider;

    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log($"Player body {other} has come in contact with {gameObject}");
        }

        if (other.tag == "PlayerFeet")
        {
            Debug.Log($"Player feet {other} has come in contact with {gameObject}");
        }
    }

    void InflictDamage()
    {

    }
}
