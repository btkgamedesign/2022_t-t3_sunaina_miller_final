using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InkEnemyBehaviour : MonoBehaviour
{
    [SerializeField] float movementSpeed;

    void Start()
    {
        
    }

    void Update()
    {
        transform.Translate(Vector3.left * movementSpeed * Time.deltaTime); //enemy moving to left
    }
}
