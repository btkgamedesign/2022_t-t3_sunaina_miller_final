using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score : MonoBehaviour
{
    public static int scoreValue = 0;
    public TMP_Text scoreText;

    void Update()
    {
        scoreText.text = scoreValue.ToString(); //displaying current score in UI
    }
}
